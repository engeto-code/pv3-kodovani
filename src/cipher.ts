export const caesarCipher = (str: string, shift: number) => {
    return str
        .toUpperCase()
        .split('')
        .map((char: string) => {
            const code = char.charCodeAt(0) + shift;
            if (code > 90) {
                return String.fromCharCode(code - 26);
            }
            return String.fromCharCode(code);
        })
        .join('');
}

export const caesarDecipher = (str: string, shift: number) => {
    return str
        .toUpperCase()
        .split('')
        .map((char: string) => {
            const code = char.charCodeAt(0) - shift;
            if (code < 65) {
                return String.fromCharCode(code + 26);
            }
            return String.fromCharCode(code);
        })
        .join('');
}

export const vigenereCipher = (str: string, key: string) => {
    return str
        .toUpperCase()
        .split('')
        .map((char: string, i: number) => {
            const code = char.charCodeAt(0) + key.charCodeAt(i % key.length) - 65;
            if (code > 90) {
                return String.fromCharCode(code - 26);
            }
            return String.fromCharCode(code);
        })
        .join('');
}

export const vigenereDecipher = (str: string, key: string) => {
    return str
        .toUpperCase()
        .split('')
        .map((char: string, i: number) => {
            const code = char.charCodeAt(0) - key.charCodeAt(i % key.length) + 65;
            if (code < 65) {
                return String.fromCharCode(code + 26);
            }
            return String.fromCharCode(code);
        })
        .join('');
}

const keyMap = new Map<string, string>();
keyMap.set('A', 'Z');
keyMap.set('B', 'Y');
keyMap.set('C', 'X');
keyMap.set('D', 'W');
keyMap.set('E', 'V');
keyMap.set('F', 'U');
keyMap.set('G', 'T');
keyMap.set('H', 'S');
keyMap.set('I', 'R');
keyMap.set('J', 'Q');
keyMap.set('K', 'P');
keyMap.set('L', 'O');
keyMap.set('M', 'N');
keyMap.set('N', 'M');
keyMap.set('O', 'L');
keyMap.set('P', 'K');
keyMap.set('Q', 'J');
keyMap.set('R', 'I');
keyMap.set('S', 'H');
keyMap.set('T', 'G');
keyMap.set('U', 'F');
keyMap.set('V', 'E');
keyMap.set('W', 'D');
keyMap.set('X', 'C');
keyMap.set('Y', 'B');
keyMap.set('Z', 'A');

export const dictionaryCipher = (str: string, key: Map<string, string> = keyMap) => {
    return str
        .toUpperCase()
        .split('')
        .map((char: string) => {
            return key.get(char) || char;
        })
        .join('');
}

export const dictionaryDecipher = (str: string, key: Map<string, string> = keyMap) => {
    return str
        .toUpperCase()
        .split('')
        .map((char: string) => {
            return key.get(char) || char;
        })
        .join('');
}