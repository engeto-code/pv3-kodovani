import { useState } from 'react'
import './App.css'
import { caesarCipher, dictionaryCipher} from './cipher'

const key = "abcdefghijklmnopqrstuvwxyz"

function App() {
  const [word, setWord] = useState<string>("")

  return (
    <>
      <div className="input">
        <input
          type="text"
          value={word}
          onChange={(e) => {
            if (e.target.value.length > 10) {
              alert("Max 10 characters")
              return
            }
            setWord(e.target.value)
          }}
        />
      </div>
      {word.length > 0 && <><div className="letter-container">
        {word.split("").map((letter, index) => (
          <div className="letter" key={index}>
            <p className='char'>{letter}</p>
            <p className='ord'>{letter.charCodeAt(0)}</p>
            <p className='hex'>{"0xF1" + letter.charCodeAt(0).toString(16)}</p>
          </div>
        ))}
      </div>
      <div className='ciphertext'>
        <p>Caesarova šifra (5): {caesarCipher(word, 5)}</p>
        <p>Transpoziční šifra ("{key}"): {dictionaryCipher(word)}</p>
      </div></>}
    </>
  )
}

export default App
