# 3. Portfolio výzva

Zdra víčko!

Další výzva pro vás je na světě!
Vaším úkolem bude trošku prozkoumat, jak jsou znaky reprezentovány v počítači (jako čísla) a co se s tím dá provádět. Vytvoříte si také jednoduchou šifru (nebo dvě) :)

## Zadání
- Koukněte se, co je ASCII tabulka a co je UTF
- **Vytvořte vstupní pole**, přijímající **max 12 znaků**
- Kontrolujte změny pole a v případě, že bude obsahovat 1 a více znaků, pro každý z nich vypiště pod sebe tyto hodnoty
  1. Daný znak
  2. Jeho ordinální hodnotu (číselnou reprezentaci znaku)
  3. Hodnotu v **hexadecimálním tvaru** - např. `0xF16c`
- Jednotlivá písmena mají hodnoty vedle sebe, aby bylo možné je vizuálně porovnat

- Vytvořte funkci, která pod výsledkem vypíše vstup zašifrovaný pomocí tzv. [**Caesarovy šifry**](https://algoritmy.net/article/34/Caesarova-sifra)
- Vytvořte funkci, která zašifruje text pomocí vlastního definovaného slovníku, jako např.
``` js
const keys = {
  a: c
  b: a
  c: b
  //...
}
```
- Vezměte výsledek šifrování a znovu jej zašifrujte pomocí svého slovníku.
- Postup opakujte, použijte cyklus nebo rekurziví volání (s podmínkou na konci), hrejte si a sledujte, co se děje. Zkuste zkombinovat různé způsoby šifrování dohromady
- **Bavte se** :)

### Vhodné i pro Pythonisty, Javisty etc.